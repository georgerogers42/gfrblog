module bitbucket.org/georgerogers42/gfrblog

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/lawrencewoodman/roveralls v0.0.0-20171119193843-51b78509b607 // indirect
)
