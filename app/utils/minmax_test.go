package utils

import (
	"testing"
)

func TestMin(t *testing.T) {
	a := Min(0, 1)
	if a != 0 {
		t.Error("Min(0, 1) != 0", a)
	}
	b := Min(1, 0)
	if b != 0 {
		t.Error("Min(0, 1) != 0", a)
	}
}

func TestMax(t *testing.T) {
	a := Max(0, 1)
	if a != 1 {
		t.Error("Max(0, 1) != 0", a)
	}
	b := Max(1, 0)
	if b != 1 {
		t.Error("Max(0, 1) != 0", a)
	}
}
