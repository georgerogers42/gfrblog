package app

import (
	"github.com/gorilla/mux"
)

var App = mux.NewRouter()

func init() {
	App.HandleFunc("/", Index).Methods("GET")
	App.HandleFunc("/p/{id}", Index).Methods("GET")
	App.HandleFunc("/a/{slug}", Article).Methods("GET")
}
