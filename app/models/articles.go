package models

import (
	"bitbucket.org/georgerogers42/gfrblog/app/utils"
	"path/filepath"
	"sort"
)

var ArticleList = MustLoadArticles("articles/*.art.html")

var ArticleMap = ArticleList.ArticleMap()

type ArticlesMap map[string]*Article

func (a ArticlesMap) String() string {
	b := []byte{}
	for s, art := range a {
		b = append(b, s+": "+art.Title+"\n"...)
	}
	return string(b)
}

type Articles []*Article

func MustLoadArticles(glob string) Articles {
	articles, err := LoadArticles(glob)
	if err != nil {
		panic(err)
	}
	return articles
}

func LoadArticles(globs ...string) (Articles, error) {
	articles := Articles{}
	for _, glob := range globs {
		fnames, err := filepath.Glob(glob)
		if err != nil {
			return nil, err
		}
		for _, fname := range fnames {
			article, err := LoadArticle(fname)
			if err != nil {
				return nil, err
			}
			articles = append(articles, article)
		}
	}
	sort.Sort(articles)
	return articles, nil
}

func (a Articles) String() string {
	b := []byte{}
	for i := range a {
		b = append(b, a[i].String()+"\n"...)
	}
	return string(b)
}

func (a Articles) ArticleMap() ArticlesMap {
	am := ArticlesMap{}
	for _, art := range a {
		am[art.Slug] = art
	}
	return am
}

var page = 10

func (a Articles) Page(i int) Articles {
	n := i * page
	return a[n:utils.Min(len(a), n+page)]
}

func (a Articles) Pages() int {
	return utils.Max(utils.Max(len(a), 1)/page, 1)
}

func (a Articles) Len() int {
	return len(a)
}

func (a Articles) Less(i, j int) bool {
	return a[i].CreatedAt.After(a[j].CreatedAt)
}

func (a Articles) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}
