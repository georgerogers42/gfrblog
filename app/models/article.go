package models

import (
	"bufio"
	"encoding/json"
	"html/template"
	"os"
	"time"
)

type Article struct {
	Metadata
	Contents string
}

func (a *Article) HtmlContents() template.HTML {
	return template.HTML(a.Contents)
}

func LoadArticle(fname string) (*Article, error) {
	a := &Article{}
	f, err := os.Open(fname)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	s := bufio.NewScanner(f)
	hdr := []byte{}
	for s.Scan() {
		if s.Text() == "" {
			break
		}
		hdr = append(hdr, s.Bytes()...)
		hdr = append(hdr, '\n')
	}
	if err := s.Err(); err != nil {
		return nil, err
	}
	json.Unmarshal(hdr, &a.Metadata)
	contents := []byte{}
	for s.Scan() {
		contents = append(contents, s.Bytes()...)
		contents = append(contents, '\n')
	}
	if err := s.Err(); err != nil {
		return nil, err
	}
	a.Contents = string(contents)
	return a, nil
}

type Metadata struct {
	Title, Slug, Author  string
	CreatedAt, UpdatedAt time.Time
}

func (m *Metadata) String() string {
	return m.Slug
}
