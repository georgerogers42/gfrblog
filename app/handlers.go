package app

import (
	"bitbucket.org/georgerogers42/gfrblog/app/models"
	"fmt"
	"github.com/gorilla/mux"
	"html/template"
	"math"
	"net/http"
)

var layoutTpl = template.Must(template.ParseFiles("templates/layout.tmpl"))

var indexTpl = template.Must(template.Must(layoutTpl.Clone()).ParseFiles("templates/index.tmpl"))

func Index(w http.ResponseWriter, r *http.Request) {
	sid, e := mux.Vars(r)["id"]
	id := 0
	if _, err := fmt.Sscanf(sid, "%d", &id); e && err != nil {
		http.NotFound(w, r)
		return
	}
	env := map[string]interface{}{}
	if id >= models.ArticleList.Pages() {
		http.NotFound(w, r)
		return
	}
	env["Articles"] = models.ArticleList.Page(id)
	env["Pre"] = int(math.Min(float64(id)+1, float64(models.ArticleList.Pages()-1)))
	env["Nxt"] = int(math.Min(float64(id)+1, 0))
	indexTpl.Execute(w, env)
}

var articleTpl = template.Must(template.Must(layoutTpl.Clone()).ParseFiles("templates/article.tmpl"))

func Article(w http.ResponseWriter, r *http.Request) {
	slug := mux.Vars(r)["slug"]
	env := map[string]interface{}{}
	if article, ok := models.ArticleMap[slug]; ok {
		env["Article"] = article
	} else {
		http.NotFound(w, r)
		return
	}
	articleTpl.Execute(w, env)
}
