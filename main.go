package main

import (
	"bitbucket.org/georgerogers42/gfrblog/app"
	"fmt"
	"log"
	"net/http"
	"os"
)

func main() {
	host := os.Getenv("HOST")
	if host == "" {
		host = "0.0.0.0"
	}
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	on := fmt.Sprintf("%s:%s", host, port)
	http.Handle("/", app.App)
	http.Handle("/static/", http.FileServer(http.Dir("public")))
	http.HandleFunc("/favicon.ico", faviconHandler)
	log.Printf("Listening on: %s", on)
	log.Fatal(http.ListenAndServe(on, nil))
}

func faviconHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "public/favicon.ico")
}
